package edu.ntnu.idatt2001;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;

import static org.junit.jupiter.api.Assertions.*;
class DepartmentTest {

    //Arrange
    Department testDepartment = new Department("testDepartment");
    Employee testEmployee = new Employee("testEmployeeFirstname", "testEmployeeLastname", "123");
    Patient testPatient = new Patient("testPatientFirstname", "testPatientLastname", "456");


    @org.junit.jupiter.api.Test
    void removeExistingEmployee() {
        //Arrange
        testDepartment.addEmployee(testEmployee);

        //Act
        try{
            testDepartment.remove(new Employee("testEmployeeFirstname", "testEmployeeLastname", "123"));
        } catch (RemoveException error) {
            error.printStackTrace();
            System.exit(1);
        }

        //Assert
        //Checking if the employee registry is empty after adding and removing the same employee
        assertTrue(testDepartment.getEmployees().isEmpty());

    }

    @org.junit.jupiter.api.Test
    void removeExistingPatient(){
        //Arrange
        testDepartment.addPatient(testPatient);

        //Act
        try{
            testDepartment.remove(new Patient("testPatientFirstname", "testPatientLastname", "456"));
        } catch (RemoveException error) {
            error.printStackTrace();
            System.exit(1);
        }

        //Assert
        //Checking if the patient registry is empty after adding and removing the same patient
        assertTrue(testDepartment.getPatients().isEmpty());
    }

    @org.junit.jupiter.api.Test
    void  removeNonExistingPerson(){
        //checking first to see if the the patient we will try to remove exists in the department
        assertFalse(testDepartment.getPatients().contains(testDepartment.getPatients().indexOf(testPatient)));

        //trying to remove the non-existing patient, should give RemoveException error
        try{
            testDepartment.remove(testPatient);
        } catch (RemoveException error) {
            error.printStackTrace();
            System.exit(1);
        }

    }

}