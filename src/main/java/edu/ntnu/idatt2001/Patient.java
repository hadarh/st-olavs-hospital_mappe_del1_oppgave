package edu.ntnu.idatt2001;

/**
 * Class Patient
 * @version 1.01 2021-02-24
 * @author Hadar Hayat
 */

public class Patient extends Person implements Diagnosable{

    private String diagnosis;

    /**
     * Constructor that creates new Patient object, with all required parameters
     *
     * @param firstname
     * @param lastname
     * @param socialSecurityNumber
     */
    public Patient(String firstname, String lastname, String socialSecurityNumber){
        super(firstname,lastname,socialSecurityNumber);
    }

    /**
     * set diagnosis for this patient
     * @see getDiagnosis
     *
     * @return returns the diagnosis of the patient
     */
    public String getDiagnosis(){
        return diagnosis;
    }

    /**
     * @see setDiagnosis
     *
     * @param diagnosisInput
     */
    public void setDiagnosis(String diagnosisInput){
        this.diagnosis = diagnosisInput;
    }
}
