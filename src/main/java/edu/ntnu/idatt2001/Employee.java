package edu.ntnu.idatt2001;

/**
 * Class Employee
 * @version 1.01 2021-02-24
 * @author Hadar Hayat
 */

public class Employee extends Person{

    /**
     * Constructor that creates new Employee object, with all required parameters
     *
     * @param firstname
     * @param lastname
     * @param socialSecurityNumber
     */
    public Employee(String firstname, String lastname, String socialSecurityNumber){
        super(firstname,lastname,socialSecurityNumber);
    }

    /**
     * @see getTitle
     *
     * @return Return the title of the employee, in this case being simply Employee
     */
    public String getTitle(){
        return "Employee";
    }
}
