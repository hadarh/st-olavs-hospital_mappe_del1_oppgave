package edu.ntnu.idatt2001;

/**
 * Abstract class Person
 * @version 1.01 2021-02-24
 * @author Hadar Hayat
 */

public abstract class Person {

    private String firstname;
    private String lastname;
    private String socialSecurityNumber;

    /**
     * Constructor for abstract class Person, inherited by many other classes to create either new Employee or new Patient object, with all required parameters
     *
     * @param firstname
     * @param lastname
     * @param socialSecurityNumber
     */
    public Person(String firstname, String lastname, String socialSecurityNumber){
        this.firstname = firstname;
        this.lastname = lastname;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * @see setFirstname
     *
     * @param  firstnameInput
     */
    public void setFirstname(String firstnameInput){this.firstname = firstnameInput;}

    /**
     * @see setLastname
     *
     * @param  lastnameInput
     */
    public void setLastname(String lastnameInput){this.lastname = lastnameInput;}

    /**
     * @see setSocialSecurityNumber
     *
     * @param  socialSecurityNumberInput
     */
    public void setSocialSecurityNumber(String socialSecurityNumberInput){this.socialSecurityNumber = socialSecurityNumberInput;}

    /**
     * @see getFirstname
     *
     * @return Return the first name of either an employee or patient
     */
    protected String getFirstname(){return firstname;}

    /**
     * @see getLastname
     *
     * @return Return the last name of either an employee or patient
     */
    protected String getLastname(){return lastname;}

    /**
     * @see getSocialSecurityNumber
     *
     * @return Return the social security number of either an employee or patient
     */
    protected String getSocialSecurityNumber(){return socialSecurityNumber;}

    protected String getFullName(){return firstname+" "+lastname;}

}
