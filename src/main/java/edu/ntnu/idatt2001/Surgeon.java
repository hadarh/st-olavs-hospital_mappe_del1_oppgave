package edu.ntnu.idatt2001;

/**
 * Class Surgeon
 * @version 1.01 2021-02-24
 * @author Hadar Hayat
 */

public final class Surgeon extends Doctor{


    /**
     * Constructor that creates new Surgeon object, with all required parameters
     *
     * @param firstname
     * @param lastname
     * @param socialSecurityNumber
     */
    public Surgeon(String firstname, String lastname, String socialSecurityNumber){
        super(firstname,lastname,socialSecurityNumber);
    }

    @Override
    /**
     * set diagnosis for specified patient
     * @see setDiagnosis
     *
     * @param  patient
     * @param diagnosis
     */
    public void setDiagnosis(Patient patient, String diagnosis){
        patient.setDiagnosis(diagnosis);
    }


    @Override
    /**
     * @see getTitle
     *
     * @return Return the title of the employee, in this case being Surgeon (Doctor)
     */
    public String getTitle() {
        return "Surgeon (Doctor)";
    }
}
