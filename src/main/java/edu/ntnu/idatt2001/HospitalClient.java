package edu.ntnu.idatt2001;

public class HospitalClient {
    public static void main(String[] args) {
        Hospital hospital = new Hospital("St. Olav Hospital");

        HospitalTestData.fillRegisterWithTestData(hospital);
        Department emergency = hospital.getDepartments().get(0);

        try{
            emergency.remove(new Employee("Odd Even", "Primtallet", ""));
        } catch (RemoveException error) {
            error.printStackTrace();
            System.exit(1);
        }

        try{
            emergency.remove(new Patient("Non", "Existing", ""));
        } catch (RemoveException error) {
            error.printStackTrace();
            System.exit(1);
        }

    }
}
