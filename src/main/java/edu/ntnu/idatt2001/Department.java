package edu.ntnu.idatt2001;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Class Department
 * @version 1.01 2021-02-24
 * @author Hadar Hayat
 */

public class Department {

    private String departmentName;
    private ArrayList<Patient> patients;
    private ArrayList<Employee> employees;

    /**
     * Constructor for class Department, with all required parameters
     *
     * @param departmentName
     */
    public Department(String departmentName){
        this.departmentName = departmentName;
        this.patients = new ArrayList<Patient>();
        this.employees = new ArrayList<Employee>();
    }

    /**
     * @see setDepartmentName
     *
     * @param  departmentName
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * @see getDepartmentName
     *
     * @return  return the name of the department
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * @see getEmployees
     *
     * @return  returns employees
     */
    public ArrayList<Employee> getEmployees(){
        return employees;
    }

    /**
     * @see addEmployee
     *
     * @param employeeInput
     */
    public void addEmployee(Employee employeeInput){

        if(employees.size()==0){
            this.employees.add(employeeInput);
            System.out.println("Employee successfully added.");
        }else{
            for(Employee employee : employees){
                if (employee.getFirstname().equals(employeeInput.getFirstname()) &&
                        employee.getLastname().equals(employeeInput.getLastname()) &&
                        employee.getSocialSecurityNumber().equals(employeeInput.getSocialSecurityNumber())){
                    System.out.println("Employee already exists in the register.");
                    break;
                }else{
                    this.employees.add(employeeInput);
                    System.out.println("Employee successfully added.");
                    break;
                }
            }
        }
    }

    /**
     * @see getPatients
     *
     * @return patients
     */
    public ArrayList<Patient> getPatients(){
        return patients;
    }

    /**
     * @see addPatient
     *
     * @param patientInput
     */
    public void addPatient(Patient patientInput){

        if(patients.size()==0){
            this.patients.add(patientInput);
            System.out.println("Patient successfully added.");
        }else{
            for(Patient patient : patients){
                if (patient.getFirstname().equals(patientInput.getFirstname()) &&
                        patient.getLastname().equals(patientInput.getLastname()) &&
                        patient.getSocialSecurityNumber().equals(patientInput.getSocialSecurityNumber())){
                    System.out.println("Patient already exists in the register.");
                    break;
                }else{
                    this.patients.add(patientInput);
                    System.out.println("Patient successfully added.");
                    break;
                }
            }
        }
    }

    /**
     * @see listAllEmployees
     * Lists all employees
     */
    public void listAllEmployees() {
        if (0 == this.employees.size()) {
            System.out.println("The employees register is empty..");
        }else {
            for (int i = 0; i < this.employees.size(); i++) {
                displayEmployee(this.employees.get(i));
            }
        }
    }

    /**
     * @see listAllPatients
     * Lists all patients
     */
    public void listAllPatients() {
        if (0 == this.patients.size()) {
            System.out.println("The patients register is empty..");
        }else {
            for (int i = 0; i < this.patients.size(); i++) {
                displayPatient(this.patients.get(i));
            }
        }
    }

    /**
     * @see displayEmployee
     *
     * employee
     */
    private void displayEmployee(Employee employee) {
        System.out.println("Title                  : " + employee.getTitle());
        System.out.println("Employee Firstname      : " + employee.getFirstname());
        System.out.println("Employee Lastname       : " + employee.getLastname());
        System.out.println("Social Security number : " + employee.getSocialSecurityNumber());
        System.out.println(" ------------------------- ");
    }

    /**
     * @see displayPatients
     *
     * patient
     */
    private void displayPatient(Patient patient) {
        System.out.println("Patient Firstname      : " + patient.getFirstname());
        System.out.println("Patient Lastname       : " + patient.getLastname());
        System.out.println("Social Security number : " + patient.getSocialSecurityNumber());
        System.out.println("Diagnosis   : " + patient.getDiagnosis());
        System.out.println(" ------------------------- ");
    }

    /**
     * remove specified person
     * @see remove
     *
     * @param person
     */

    public void remove(Person person) throws RemoveException{

        //the remove method is quite repetitive, but this ensures that the method works
        // properly nonetheless of the patient, employee or both registries being completely empty

        if(patients.size()==0 && employees.size()==0){
            throw new RemoveException(person.getSocialSecurityNumber());

        } else if(patients.size()==0){
            for(Employee employee : employees){
                if (person.getFirstname().equals(employee.getFirstname()) && person.getLastname().equals(employee.getLastname()) &&
                        person.getSocialSecurityNumber().equals(employee.getSocialSecurityNumber())) {
                    employees.remove(employee);
                    System.out.println("Employee removed from register.");
                    break;
                }else{
                    throw new RemoveException(person.getSocialSecurityNumber());
                }
            }
        }else if(employees.size()==0){
            for(Patient patient : patients){
                if (patient.getFirstname().equals(person.getFirstname()) && patient.getLastname().equals(person.getLastname()) &&
                        patient.getSocialSecurityNumber().equals(person.getSocialSecurityNumber())) {
                    patients.remove(patient);
                    System.out.println("Patient removed from register.");
                    break;
                }else{
                    throw new RemoveException(person.getSocialSecurityNumber());
                }
            }

        }else if(patients.size()>0 && employees.size()>0){
            for(Employee employee : employees){
                if (employee.getFirstname().equals(person.getFirstname()) && employee.getLastname().equals(person.getLastname()) &&
                        employee.getSocialSecurityNumber().equals(person.getSocialSecurityNumber())) {
                    employees.remove(employee);
                    System.out.println("Employee removed from register.");
                    break;
                }else {
                    for (Patient patient : patients){
                        if (patient.getFirstname().equals(person.getFirstname()) && patient.getLastname().equals(person.getLastname()) &&
                                patient.getSocialSecurityNumber().equals(person.getSocialSecurityNumber())) {
                            patients.remove(patient);
                            System.out.println("Patient removed from register.");
                            break;
                        } else {
                            throw new RemoveException(person.getSocialSecurityNumber());
                        }
                    }
                }
            }

        }

    }


}
