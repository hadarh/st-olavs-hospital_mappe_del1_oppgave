package edu.ntnu.idatt2001;

/**
 * Abstract class Doctor
 * @version 1.01 2021-02-24
 * @author Hadar Hayat
 */

public abstract class Doctor extends Employee{

    /**
     * Constructor for abstract class Doctor, inherited by other classes to create either new Surgeon or new GeneralPractitioner object, with all required parameters
     *
     * @param firstname
     * @param lastname
     * @param socialSecurityNumber
     */
    public Doctor(String firstname, String lastname, String socialSecurityNumber){
        super(firstname,lastname,socialSecurityNumber);
    }

    /**
     * @see setDiagnosis
     *
     * @param  patient
     * @param diagnosis
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);

    /**
     * @see getTitle
     */
    public abstract String getTitle();

    }
