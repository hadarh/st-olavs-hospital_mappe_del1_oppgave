package edu.ntnu.idatt2001;

/**
 * Class GeneralPractitioner
 * @version 1.01 2021-02-24
 * @author Hadar Hayat
 */

public final class GeneralPractitioner extends Doctor{

    /**
     * Constructor that creates new GeneralPractitioner object, with all required parameters
     *
     * @param firstname
     * @param lastname
     * @param socialSecurityNumber
     */
    public GeneralPractitioner(String firstname, String lastname, String socialSecurityNumber){
        super(firstname,lastname,socialSecurityNumber);
    }

    @Override
    /**
     * set diagnosis for specified
     * @see setDiagnosis
     *
     * @param  patient
     * @param diagnosis
     */
    public void setDiagnosis(Patient patient, String diagnosis){
        patient.setDiagnosis(diagnosis);
    }

    @Override
    /**
     * @see getTitle
     *
     * @return Return the title of the employee, in this case being General Practitioner (Doctor)
     */
    public String getTitle(){
        return "General Practitioner (Doctor)";
    }
}
