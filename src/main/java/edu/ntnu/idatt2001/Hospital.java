package edu.ntnu.idatt2001;

import java.util.ArrayList;

/**
 * Class Department
 * @version 1.01 2021-02-24
 * @author Hadar Hayat
 */
public class Hospital {
    private String hospitalName;
    private ArrayList<Department> departments;

    /**
     * Constructor for class Hospital, with all required parameters
     *
     * @param hospitalName
     */
    public Hospital(String hospitalName){
        this.hospitalName = hospitalName;
        this.departments = new ArrayList<Department>();
    }

    /**
     * @see getHospitalName
     *
     * @return  returns the name of the hospital
     */
    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * @see getDepartments
     *
     * @return  returns departments
     */
    public ArrayList<Department> getDepartments(){
        return departments;
    }

    /**
     * add new department
     * @see addDepartment
     *
     * @param departmentInput
     */
    public void addDepartment(Department departmentInput){
        if(departments.size()==0){
            this.departments.add(departmentInput);
            System.out.println("Department successfully added.");
        }else{
            for(Department department : departments){
                if (department.getDepartmentName().equals(departmentInput.getDepartmentName())){
                    System.out.println("Department already exists in the register.");
                    break;
                }else{
                    this.departments.add(departmentInput);
                    System.out.println("Department successfully added.");
                    break;
                }
            }
        }

    }

    /**
     * list all departments in a hospital
     * @see listAllDepartments
     */
    public void listAllDepartments() {
        //check if department registry is empty
        if (0 == this.departments.size()) {
            System.out.println("The department register is empty..");
        }else {
            for (int i = 0; i < this.departments.size(); i++) {
                displayDepartment(this.departments.get(i));
            }
        }
    }

    /**
     * @see displayDepartment
     *
     * @param department
     */
    private void displayDepartment(Department department) {
        System.out.println("\nDepartment      : " + department.getDepartmentName());
        System.out.println("\nEmployees       :\n"); department.listAllEmployees();
        System.out.println("\nPatients        :\n"); department.listAllPatients();
        System.out.println(" ------------------------- ");
    }
}

