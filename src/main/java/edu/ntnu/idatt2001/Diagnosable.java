package edu.ntnu.idatt2001;
/**
 * Interface Diagnosable
 * @version 1.01 2021-02-24
 * @author Hadar Hayat
 */

public interface Diagnosable {

    /**
     * set new diagnosis
     * @see setDiagnosis
     *
     * @param diagnosis
     */
    public void setDiagnosis(String diagnosis);
}
