package edu.ntnu.idatt2001;

/**
 * Exception class RemoveException
 * @version 1.01 2021-02-24
 * @author Hadar Hayat
 */

public class RemoveException extends Exception{

    /**
     *
     * @param personSocialNumber
     */
    public RemoveException(String personSocialNumber){
        super("The person you are trying to remove does not exist in our register. \n" + personSocialNumber + " non existent.");
    }
}
