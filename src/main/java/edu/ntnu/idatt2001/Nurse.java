package edu.ntnu.idatt2001;

/**
 * Class Nurse
 * @version 1.01 2021-02-24
 * @author Hadar Hayat
 */

public final class Nurse extends Employee{

    /**
     * Constructor that creates new Nurse object, with all required parameters
     *
     * @param firstname
     * @param lastname
     * @param socialSecurityNumber
     */
    public Nurse(String firstname, String lastname, String socialSecurityNumber){
        super(firstname,lastname,socialSecurityNumber);
    }

    @Override
    /**
     * @see getTitle
     *
     * @return Return the title of the employee, in this case being Nurse
     */
    public String getTitle() {
        return "Nurse";
    }
}
